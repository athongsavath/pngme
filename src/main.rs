#[macro_use]
extern crate clap;

mod args;
mod chunk;
mod chunk_type;
mod commands;
mod png;

pub type Error = Box<dyn std::error::Error>;
pub type Result<T> = std::result::Result<T, Error>;

use clap::{App, Arg, SubCommand};

fn main() -> Result<()> {
    let matches = App::new("pngme")
        .author(crate_authors!())
        .about("My implementation of pngme.")
        .subcommand(
            SubCommand::with_name("encode")
                .about("Encode given file with given string.")
                .arg(Arg::with_name("file_path").required(true))
                .arg(Arg::with_name("chunk_type").required(true))
                .arg(Arg::with_name("message").required(true))
                .arg(Arg::with_name("output_file").required(false)),
        )
        .subcommand(
            SubCommand::with_name("decode")
                .about("Decode the message from the given file")
                .arg(Arg::with_name("file_path").required(true))
                .arg(Arg::with_name("chunk_type").required(true)),
        )
        .subcommand(
            SubCommand::with_name("remove")
                .about("Remove the chunk type from the given file")
                .arg(Arg::with_name("file_path").required(true))
                .arg(Arg::with_name("chunk_type").required(true)),
        )
        .subcommand(
            SubCommand::with_name("print")
                .about("Print the contents from the given file")
                .arg(Arg::with_name("file_path").required(true)),
        )
        .get_matches();

    if let Some(matches) = matches.subcommand_matches("encode") {
        let file_path = matches.value_of("file_path").unwrap();
        let chunk_type = matches.value_of("chunk_type").unwrap();
        let message = matches.value_of("message").unwrap();
        let output_file = matches.value_of("output_file");
    }

    if let Some(matches) = matches.subcommand_matches("decode") {
        let file_path = matches.value_of("file_path").unwrap();
        let chunk_type = matches.value_of("chunk_type").unwrap();
    }

    if let Some(matches) = matches.subcommand_matches("remove") {
        let file_path = matches.value_of("file_path").unwrap();
        let chunk_type = matches.value_of("chunk_type").unwrap();
    }

    if let Some(matches) = matches.subcommand_matches("print") {
        let file_path = matches.value_of("file_path").unwrap();
    }
    Ok(())
}
