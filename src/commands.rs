use std::path::PathBuf;

enum Command {
    Encode(EncodeArgs),
    Decode(DecodeArgs),
    Remove(RemoveArgs),
    Print(PrintArgs),
}

struct EncodeArgs {
    file_path: PathBuf,
    chunk_type: String,
    message: String,
    output_file: Option<PathBuf>,
}

struct DecodeArgs {
    file_path: PathBuf,
    chunk_type: String,
}

struct RemoveArgs {
    file_path: PathBuf,
    chunk_type: String,
}

struct PrintArgs {
    file_path: PathBuf,
}
