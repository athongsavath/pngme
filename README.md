# pngme
My implementation of pngme, which can be found [here](https://picklenerd.github.io/pngme_book/introduction.html).

# What are we making?
> 
>We're making a command line program that lets you hide secret messages in PNG files. Your program will have four commands:
>
>1. Encode a message into a PNG file
>2. Decode a message stored in a PNG file
>3. Remove a message from a PNG file
>4. Print a list of PNG chunks that can be searched for messages
>
> -- from the [project page](https://picklenerd.github.io/pngme_book/introduction.html#what-are-we-making)
 